import RoleModel from "../models/roles.model";
import { StatusCodes } from "http-status-codes";
import {
  paginationFields,
  paginationSerialize,
} from "../helpers/pagination.helper";
import { RoleNotFound } from "../errors/roles.error";

export default class RoleController {
  constructor() {
    this.model = RoleModel;
  }

  // https://sequelize.org/docs/v6/core-concepts/model-querying-finders/#findandcountall
  async fetchAll(req, res) {
    try {
      const { page, per_page, q: search } = req.query;
      // limit | offset
      // Formula (page > 1) | (page <= 1) offset 0
      // page * per_page - per_page
      // =============================
      // 10 | 0 -> Pagina 1
      // 10 | 10 -> Pagina 2
      // 10 | 20 -> Pagina 3
      const { limit, offset } = paginationFields(page, per_page);

      const filters = { status: true };

      if (search) {
        filters["$or"] = [
          {
            name: new RegExp(search, "i"),
          },
        ];
      }

      // 1era Forma
      // const records = await this.model.find(filters).skip(offset).limit(limit);
      // const recordsCount = await this.model.countDocuments(filters);

      // 2da Forma
      // https://www.mongodb.com/docs/manual/aggregation/
      // https://www.mongodb.com/docs/manual/reference/operator/aggregation/facet/
      const aggregationPipeline = [
        {
          $match: filters,
        },
        {
          $facet: {
            metadata: [{ $count: "total" }],
            rows: [{ $skip: offset }, { $limit: limit }],
          },
        },
        {
          $project: {
            metadata: 1,
            rows: 1,
            adjustedCount: {
              $cond: {
                if: { $eq: [{ $size: "$rows" }, 0] },
                then: 0,
                else: { $arrayElemAt: ["$metadata.total", 0] },
              },
            },
          },
        },
      ];

      const [records] = await this.model.aggregate(aggregationPipeline);
      const { adjustedCount: totalDocuments, rows } = records;

      return res
        .status(StatusCodes.OK)
        .json(
          paginationSerialize({ rows, count: totalDocuments }, page, per_page)
        );
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async save(req, res) {
    try {
      const { body } = req;
      const record = this.model(body);
      await record.save();
      return res.status(StatusCodes.CREATED).json(record);
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async fetchById(req, res) {
    try {
      const { id } = req.params;
      const record = await this._findRoleById(id);

      if (!record) {
        throw new RoleNotFound();
      }

      return res.status(StatusCodes.OK).json(record);
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async update(req, res) {
    try {
      const { body, params } = req;
      const { id } = params;
      const record = await this._findRoleById(id);

      if (!record) {
        throw new RoleNotFound();
      }

      await record.updateOne(body);
      return res
        .status(StatusCodes.OK)
        .json({ message: `El rol ${record.name} ha sido actualizado` });
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async remove(req, res) {
    try {
      const { id } = req.params;
      const record = await this._findRoleById(id);

      if (!record) {
        throw new RoleNotFound();
      }

      await record.updateOne({ status: false });

      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async _findRoleById(id) {
    return await this.model.findOne({
      status: true,
      _id: id,
    });
  }
}
