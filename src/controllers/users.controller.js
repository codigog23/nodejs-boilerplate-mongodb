import UserModel from "../models/users.model";
import { StatusCodes } from "http-status-codes";
import {
  paginationFields,
  paginationSerialize,
} from "../helpers/pagination.helper";
import Bucket from "../providers/bucket.provider";
import { UserNotFound } from "../errors/users.error";

export default class UserController {
  constructor() {
    this.model = UserModel;
    this.bucket = new Bucket("avatar");
  }

  async fetchAll(req, res) {
    try {
      const { page, per_page, q: search } = req.query;
      const { limit, offset } = paginationFields(page, per_page);

      const filters = { status: true };

      if (search) {
        filters["$or"] = [
          {
            name: new RegExp(search, "i"),
          },
          {
            last_name: new RegExp(search, "i"),
          },
          {
            username: new RegExp(search, "i"),
          },
          {
            email: new RegExp(search, "i"),
          },
        ];
      }

      // 1er forma
      const records = await this.model
        .find(filters)
        .populate([
          {
            path: "role",
            select: ["-created_at", "-updated_at", "-status"],
          },
        ])
        .select(["-password"])
        .skip(offset)
        .limit(limit);
      const recordsCount = await this.model.countDocuments(filters);

      // 2da forma
      /*const aggregationPipeline = [
        {
          $match: filters,
        },
        {
          $facet: {
            metadata: [{ $count: "total" }],
            rows: [
              { $skip: offset },
              { $limit: limit },
              {
                $lookup: {
                  from: "roles",
                  localField: "role_code",
                  foreignField: "code",
                  as: "role",
                },
              },
              // https://www.mongodb.com/docs/manual/reference/operator/aggregation/unwind/#document-operand-with-options
              { $unwind: { path: "$role", preserveNullAndEmptyArrays: true } },
              {
                $project: {
                  password: 0,
                  "role.created_at": 0,
                  "role.updated_at": 0,
                  "role.code": 0,
                  "role.status": 0,
                },
              },
            ],
          },
        },
        {
          $project: {
            metadata: 1,
            rows: 1,
            adjustedCount: {
              $cond: {
                if: { $eq: [{ $size: "$rows" }, 0] },
                then: 0,
                else: { $arrayElemAt: ["$metadata.total", 0] },
              },
            },
          },
        },
      ];

      const [records] = await this.model.aggregate(aggregationPipeline);
      const { adjustedCount: totalDocuments, rows } = records;*/

      return res
        .status(StatusCodes.OK)
        .json(
          paginationSerialize(
            { rows: records, count: recordsCount },
            page,
            per_page
          )
        );
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async save(req, res) {
    try {
      const { body, files } = req;
      const avatarUrl = await this.bucket.uploadImage(
        files.avatar,
        body.username
      );
      body["avatar"] = avatarUrl;
      const record = this.model(body);
      await record.save();
      return res.status(StatusCodes.CREATED).json(record);
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async fetchById(req, res) {
    try {
      const { id } = req.params;
      const record = await this._findUserById(id);

      if (!record) {
        throw new UserNotFound();
      }

      return res.status(StatusCodes.OK).json(record);
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async update(req, res) {
    try {
      const { body, files, params } = req;
      const { id } = params;
      const record = await this._findUserById(id);

      if (!record) {
        throw new UserNotFound();
      }

      // Subir la imagen
      if (files?.avatar) {
        const avatarUrl = await this.bucket.uploadImage(
          files.avatar,
          record.username
        );
        body["avatar"] = avatarUrl;
      }

      await record.updateOne(body);
      return res
        .status(StatusCodes.OK)
        .json({ message: `El usuario ${record.username} ha sido actualizado` });
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async remove(req, res) {
    try {
      const { id } = req.params;
      const record = await this._findUserById(id);

      if (!record) {
        throw new UserNotFound();
      }

      await record.updateOne({ status: false });

      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (error) {
      return res.status(error?.code || StatusCodes.INTERNAL_SERVER_ERROR).json({
        message: error.message,
      });
    }
  }

  async _findUserById(id) {
    return await this.model
      .findOne({
        _id: id,
        status: true,
      })
      .select(["-password"]);
  }
}
