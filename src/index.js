import "dotenv/config";
import { ExpressConfig } from "./config/expressConfig";
import initializeDatabase from "./config/databaseConfig";

const express = new ExpressConfig();

(async () => {
  await initializeDatabase();
  await express.listen();
})();
