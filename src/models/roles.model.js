import { Schema, model } from "mongoose";

// Definimos nuestro esquema
const roleSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    code: {
      type: String,
      required: true,
      unique: true,
    },
    status: {
      type: Boolean,
      default: true,
    },
  },
  {
    collection: "roles",
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
  }
);

// Creamos el modelo
// roles -> alias para ser llamado desde otro modelo
const RoleModel = model("roles", roleSchema);
export default RoleModel;
