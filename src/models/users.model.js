import { Schema, model } from "mongoose";
import config from "../config/authConfig";
import { hashSync, compareSync } from "bcryptjs";

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    last_name: {
      type: String,
      required: true,
    },
    username: {
      type: String,
      unique: true,
      required: true,
    },
    email: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    avatar: {
      type: String,
      required: false,
    },
    role_code: {
      type: String,
      required: true,
    },
    status: {
      type: Boolean,
      default: true,
    },
  },
  {
    collection: "users",
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
    toJSON: {
      virtuals: true,
    },
    // 2da Forma
    virtuals: {
      role: {
        options: {
          ref: "roles",
          localField: "role_code",
          foreignField: "code",
          justOne: true,
        },
      },
    },
  }
);

// https://mongoosejs.com/docs/tutorials/virtuals.html
// 1er Forma
// userSchema.virtual("role", {
//   ref: "roles",
//   localField: "role_code",
//   foreignField: "code",
//   justOne: true,
// });

// https://mongoosejs.com/docs/middleware.html#pre
userSchema.pre("save", async function (next) {
  if (this.isModified("password")) {
    const passwordHash = hashSync(this.password, config.bcryptRounds);
    this.password = passwordHash;
  }
  next();
});

userSchema.methods.validatePassword = async function (password) {
  return compareSync(password, this.password);
};

const UserModel = model("users", userSchema);
export default UserModel;
